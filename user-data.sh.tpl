#!/bin/bash

# Disable swap
swapoff -a



# Set kernel parameters
echo "fs.inotify.max_user_watches = 524288" | tee -a /etc/sysctl.conf
echo "fs.inotify.max_user_instances = 512" | tee -a /etc/sysctl.conf
sysctl -p /etc/sysctl.conf

# Configure additionnal storage for docker
function configure_ssd {
  drive=$1
  parted -a optimal $drive mklabel gpt
  parted -a optimal $drive mkpart primary ext4 0% 100%
  mkfs.ext4 "${drive}p1"
  if [ -b "${drive}p1"  ]; then
    mount "${drive}p1" /var/lib/docker -t ext4
  fi
}

if [[ "%%RUNNER_PLAN%%" == "m3.large.x86" ]]; then
  echo -e "\n Setup disks"
  apt -y update
  apt -y install parted
  mkdir -p /var/lib/docker

  # Servers have either 2 SATA and 2 NVME disks or 4 NVME disks
  # so large disks are the 2 last NVME
  if [ -b /dev/nvme3n1 ]; then
    configure_ssd /dev/nvme2n1
    configure_ssd /dev/nvme3n1
  else
    configure_ssd /dev/nvme0n1
    configure_ssd /dev/nvme1n1
  fi
fi

echo -e "\n Create a GitLab Runner user"
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
rm /home/gitlab-runner/.bash_logout  # https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading

# Fix https://gitlab.com/gitlab-org/gitlab-runner/-/issues/38252
mkdir -p /etc/sysconfig && echo "HOME=/home/gitlab-runner" > /etc/sysconfig/gitlab-runner
# set HOME for root if not already
if [ -z "$HOME" ] && [ $(id -u) = "0" ]; then export HOME=/root; fi

echo -e "\n Install docker"
curl -fsSL https://get.docker.com | sh

# Give gitlab-runner user docker permission
usermod -aG docker gitlab-runner

echo -e "\n Install other useful packages"
# yq
wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
# python packages
apt install -y python3-junit.xml yamllint

# install additional packages required by specific projects
if [[ "%%RUNNER_HOSTNAME%%" == "sylva-core"* ]]; then
  echo -e "\n Install Helm"
  curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
fi

# install additional packages required by specific projects
if [[ "%%RUNNER_HOSTNAME%%" == "kiwi-imagebuilder"* ]]; then
  apt install -y alien rpm zypper python3-pip qemu-kvm virt-manager bridge-utils mtools
fi

echo -e "\n Setup gitlab-runner"
# Download the gitlab-runner binary
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner

# get ip address
ip_addr=$(hostname -I | awk '{print $1}')

# Install runner and run as a service
mkdir -p /etc/gitlab-runner/
cat <<EOF > /etc/gitlab-runner/config.toml
concurrent = 11

[session_server]
  listen_address = "[::]:8093"
  advertise_address = "$ip_addr:8093"
  session_timeout = 1800
EOF

gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
gitlab-runner start

# Add shell executor
gitlab-runner register --non-interactive \
    --url "https://%%CI_SERVER_HOST%%" --name "%%RUNNER_HOSTNAME%%" \
    --token=%%GITLAB_RUNNER_SHELL_TOKEN%% \
    --limit=1 \
    --executor=shell

# Add docker executor
gitlab-runner register --non-interactive \
    --url "https://%%CI_SERVER_HOST%%" --name "%%RUNNER_HOSTNAME%%-docker" \
    --token=%%GITLAB_RUNNER_DOCKER_TOKEN%% \
    --executor=docker \
    --docker-image=alpine:3.19 \
    --docker-network-mode=host \
    --limit=10 \
    --docker-privileged=true

gitlab-runner verify
