#!/usr/bin/env python3

import argparse
import json
import os
import sys
from wrappers.common import fail, ALLOWED_PROJECTS
from wrappers.equinix import EquinixWrapper
from wrappers.gitlab import GitlabWrapper
from datetime import datetime, timedelta, timezone


def get_equinix_servers():
    """
    This function prints a json object to sdtout describing running servers.
    It is particularly used for usage report web page.
    """
    equinix = EquinixWrapper()
    servers = [
        {
            "name": device["hostname"],
            "id": device["id"],
            "plan": device["plan"]["name"],
            "os": device["operating_system"]["name"],
            "metro": device["metro"]["name"],
            "created_at": device["created_at"],
            "pipeline": device["hostname"].split("-")[-1],
            "termination_time": device["termination_time"],
        }
        for device in equinix.get_servers()["devices"]
    ]
    print(f"> There is {len(servers)} running servers", file=sys.stderr)
    report = {
        "date": str(datetime.now(timezone.utc)),
        "running-servers": servers
    }
    print(json.dumps(report, indent=2))


def get_gitlab_runners():
    runners = []
    for project in ALLOWED_PROJECTS:
        gitlab = GitlabWrapper(project)
        runners += gitlab.get_runners()
    print(runners)


def create_runner():
    name = os.getenv("RUNNER_HOSTNAME")
    project_id = os.getenv("RUNNER_PROJECT_ID")
    if not name:
        fail("RUNNER_HOSTNAME not defined")
    if not project_id:
        fail("RUNNER_PROJECT_ID not defined")

    additional_tags = os.getenv("RUNNER_ADDITIONAL_TAGS", "").split(",")
    print(f"Creating runner {name} with additional tags {additional_tags}")
    equinix = EquinixWrapper()
    gitlab = GitlabWrapper(os.getenv("RUNNER_PROJECT"))

    if equinix.get_server_id_by_name(name):
        fail(f"Server {name} already exists")
    if gitlab.get_runner_id_by_name(name):
        fail(f"Runner {name} already exists")

    print("  > Creating new runner in Gitlab")
    runner = gitlab.create_runner(name, project_id, ["shell"] + additional_tags)
    os.environ["GITLAB_RUNNER_SHELL_TOKEN"] = runner.token

    print("  > Creating new docker runner in Gitlab")
    runner_docker = gitlab.create_runner((name + "-docker"), project_id, ["docker"] + additional_tags)
    os.environ["GITLAB_RUNNER_DOCKER_TOKEN"] = runner_docker.token

    plan = os.getenv("RUNNER_PLAN")
    operating_system = os.getenv("RUNNER_OS")
    print(f"  > Creating new server {name} on Equinix Metal")
    print(f"    >> Server plan = {plan}")
    print(f"    >> Server OS = {operating_system}")

    termination_delay = int(os.getenv("RUNNER_TERMINATION_DELAY", "6"))

    equinix.create_server(name, plan=plan, operating_system=operating_system, termination_delay=termination_delay)


def wait_server_to_be_ready():
    name = os.getenv("RUNNER_HOSTNAME")
    if not name:
        fail("RUNNER_HOSTNAME not defined")
    equinix = EquinixWrapper()
    equinix.wait_server_to_be_ready(name)

    server = equinix.get_server_by_name(name)
    print(f"    >> Server ID = {server['id']}")
    print(f"    >> Server IP = {server['ip_addresses'][0]['address']}")
    print(f"    >> Termination time = {server['termination_time']}")


def unregister_runner():
    if "RUNNER_HOSTNAME" not in os.environ:
        fail("RUNNER_HOSTNAME not defined")

    for name in [os.getenv("RUNNER_HOSTNAME"), (os.getenv("RUNNER_HOSTNAME") + "-docker")]:
        gitlab = GitlabWrapper(os.getenv("RUNNER_PROJECT"))
        runner_id = gitlab.get_runner_id_by_name(name)
        if runner_id:
            gitlab.unregister_runner_by_id(runner_id)


def delete_server():
    name = os.getenv("RUNNER_HOSTNAME")
    if not name:
        fail("RUNNER_HOSTNAME not defined")
    equinix = EquinixWrapper()
    equinix.delete_server_by_name(name)


def display_total_equinix_usage():
    equinix = EquinixWrapper()
    today = datetime.now(timezone.utc)
    today_usage = int(equinix.get_total_project_usage(today))
    yesterday_usage = int(equinix.get_total_project_usage(today - timedelta(days=1)) - today_usage)
    this_month_usage = int(equinix.get_total_project_usage(today.replace(day=1)))
    sliding_month_usage = int(equinix.get_total_project_usage(today - timedelta(days=30)))
    print(f"Today usage so far = ${today_usage}", file=sys.stderr)
    print(f"Yesterday usage = ${yesterday_usage}", file=sys.stderr)
    print(f"This month usage so far = ${this_month_usage}", file=sys.stderr)
    print(f"One sliding month usage = ${sliding_month_usage}", file=sys.stderr)
    report = {
        "by_day_consumption": {
            str(today): today_usage,
            str(today - timedelta(days=1)): yesterday_usage
        },
        "this_month_consumption": {str(str(today)): this_month_usage},
        "one_sliding_month_consumption": {str(str(today)): sliding_month_usage}
    }
    print(json.dumps(report, indent=2))
    print("")


def delete_old_runners():
    for project in ALLOWED_PROJECTS:
        print("****************************************")
        print(f"Removing old runners in project {project}")
        print("****************************************")
        gitlab = GitlabWrapper(project)
        runners = gitlab.get_runners()
        print("Registered runners:")
        for r in runners:
            print(f"  > {r.description} - id: {r.id}")

        old_runners = [
            r for r in runners
            if not gitlab.is_pipeline_running(
                project_id=_extract_project_from_name(r.description),
                pipeline_id=_extract_id_from_name(r.description)
            )
        ]
        print("\nRegistered runners associated to finished pipeline:")
        for r in old_runners:
            print(f"  > {r.description}")
        print("")
        for r in old_runners:
            print(f"  >> Unregistering runner {r.description}")
            gitlab.unregister_runner_by_id(r.id)


def _extract_id_from_name(name):
    return name.removesuffix("-docker").split("-")[-1]


def _extract_project_from_name(name):
    for project in ALLOWED_PROJECTS:
        if name.startswith(project.split("/")[-1]):
            return project


def delete_old_servers():
    equinix = EquinixWrapper()
    servers = equinix.get_servers()["devices"]
    print("Running servers:")
    for s in servers:
        print(f"  > {s['hostname']} - id: {s['id']} - plan: {s['plan']['name']} - metro: {s['metro']['name']} - created at {s['created_at']}")
    old_servers = []
    for s in servers:
        project_name = _extract_project_from_name(s["hostname"])
        pipeline_id = _extract_id_from_name(s["hostname"])
        if project_name:
            gitlab = GitlabWrapper(project_name)
            if not gitlab.is_pipeline_running(project_name, pipeline_id):
                old_servers.append(s)

    print("\nRunning servers associated to finished pipeline:")
    for s in old_servers:
        print(f"  > {s['hostname']}")
    print("")
    for s in old_servers:
        print(f"  >> Deleting server {s['hostname']}")
        equinix.delete_server_by_name(s['hostname'])


def test_find_best_metro():
    equinix = EquinixWrapper()
    plan = os.getenv("RUNNER_PLAN")
    metro = equinix.find_best_metro(plan)
    print(metro)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Sylva Runner Manager")
    parser.add_argument(
        "action",
        choices=[
            "get_equinix_servers",
            "get_gitlab_runners",
            "create_runner",
            "wait_server_to_be_ready",
            "unregister_runner",
            "delete_server",
            "display_total_equinix_usage",
            "delete_old_runners",
            "delete_old_servers",
            "test"
        ],
    )
    args = parser.parse_args()

    if args.action == "get_equinix_servers":
        get_equinix_servers()
    if args.action == "get_gitlab_runners":
        get_gitlab_runners()
    if args.action == "create_runner":
        create_runner()
    if args.action == "wait_server_to_be_ready":
        wait_server_to_be_ready()
    if args.action == "unregister_runner":
        unregister_runner()
    if args.action == "delete_server":
        delete_server()
    if args.action == "display_total_equinix_usage":
        display_total_equinix_usage()
    if args.action == "delete_old_runners":
        delete_old_runners()
    if args.action == "delete_old_servers":
        delete_old_servers()
    if args.action == "test":
        test_find_best_metro()
