# Runner aaS

This repo provides a way to generate "as a service" runners via cross-project pipeline triggering.

User must provided a Gitlab ID token when triggering a creation/deletion pipeline.
This token used for:

- Authenticate user which request a runner.
- Generate runner parameters:
  * Runner is associated to project from where the token is issued (source project)
  * Runner hostname is produced from source project and source pipeline ID

Notes:

- Currently runners are created on Equinix Metal infrastructure.
- User which triggers runner creation pipeline must have maintainer role on `runner-aas-interface` repo.
- Runners tags are `equinix`, `temporary` and user provided RUNNER_ADDITIONAL_TAGS.

## Usage

### Runner creation

> `.gitlab-ci.yml`

```yaml
create_runner:
  id_tokens:
    JOB_ID_TOKEN:
      aud: runner-aas
  trigger:
    project: sylva-projects/sylva-elements/ci-tooling/runner-aas-interface
    branch: main
  variables:
    REQUEST: create
    SOURCE_JWT_TOKEN: $JOB_ID_TOKEN
    RUNNER_ADDITIONAL_TAGS: my-custom-tag

```

Then runner can be used by any job using the right `tags`:

```yaml
my-job:
  tags:
    - equinix
    - my-custom-tag
  script:
    - echo "this script is executed in requested runner"
```

### Runner deletion

At this end of the pipeline, runner must be deleted via another cross-project pipeline

> `.gitlab-ci.yml`

```yaml
delete_runner:
  id_tokens:
    JOB_ID_TOKEN:
      aud: runner-aas
  trigger:
    project: sylva-projects/sylva-elements/ci-tooling/runner-aas-interface
    branch: main
  variables:
    REQUEST: delete
    SOURCE_JWT_TOKEN: $JOB_ID_TOKEN
```

## Cleaning

A pipeline is scheduled every hour to clean any Equinix server which is running for more than 3 hours.
This pipeline is also responsible for un-registering any pending runner in Gitlab.
