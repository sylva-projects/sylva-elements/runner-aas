"use strict"

// Entrypoint
//=====================
var CHART_DATA
var RUNNING_SERVER_DATA
var TODAY_CHART
var MONTH_CHART
var SLIDING_MONTH_CHART

window.onload = function () {
  init()
}

async function init() {
  TODAY_CHART = new ApexCharts(document.querySelector("#by-day-chart"), getChartOptions("By day", '#2E93fA'))
  TODAY_CHART.render()
  MONTH_CHART = new ApexCharts(document.querySelector("#by-month-chart"), getChartOptions("This month so far", '#66DA26'))
  MONTH_CHART.render()
  SLIDING_MONTH_CHART = new ApexCharts(document.querySelector("#sliding-month-chart"), getChartOptions("Sliding month", '#E91E63'))
  SLIDING_MONTH_CHART.render()
  CHART_DATA = await getJsonFileContent("consumption_history.json")
  RUNNING_SERVER_DATA = await getJsonFileContent("running_servers_report.json")
  refreshHTML()
}

async function getJsonFileContent(file) {
  const request = await fetch(file)
    .catch(function(error) {
      log('Request failed', error)
    })
  const statusCode = await request.status
  console.log("[INFO] Resquested file", file ,", get status code:", statusCode)
  const jsonResponse = await request.json()
  return jsonResponse
}

function getDataSeries(jsonData, name){
  // this function transform input jsonData
  //  "by_day_consumption": {"2023-10-29": 56.5846735838383545, "2023-10-30": 30.5348864168418648,...}
  //
  // to series as expected by Apex
  // series: [{"name": "by_day_consumption", data: [{ x: '2023-10-29', y: 56.5846735838383545 }, { x: '2023-10-30', y: 30.5348864168418648 } , ... ]}]

  let series = []
  let dataPoints = getDataPoints(jsonData[name])
  series.push({
    name: name,
    data: dataPoints
  })
  return series
}

function getDataPoints(jsonData) {
  function transformData(accumulator, key) {
    let x = key
    let y = Math.round(jsonData[key])
    accumulator.push({ x: x, y: y})

    return accumulator
  }
  const dataPoints = Object.keys(jsonData).reduce(transformData, []).sort((a, b) => Date.parse(a.x) - Date.parse(b.x))
  console.log("[INFO] Extracted dataPoints: ", dataPoints)
  return dataPoints
}

function formatRunningServerData() {
  // Handle `running_servers_report.json` to compute how long running servers are created
  const reference_date = Date.parse(RUNNING_SERVER_DATA["date"])
  const runningServers = RUNNING_SERVER_DATA["running-servers"]
  let formattedServers = []

  runningServers.forEach((server) => {
    let elem = {}
    elem["name"] = server["name"]
    elem["created at"] = server["created_at"]
    elem["created since"] = getDateDifference(Date.parse(server["created_at"]), reference_date)
    formattedServers.push(elem)
  })
  console.log("[INFO] Running servers: ", formattedServers)
  return formattedServers
}

function getDateDifference(date1, date2) {
  const durationS = Math.round((date2 - date1) / 1000)
  const _S_PER_DAYS = 60 * 60 * 24
  const _S_PER_HOURS = 60 * 60
  const _S_PER_MIN = 60
  const days = Math.floor(durationS / _S_PER_DAYS)
  const hours = Math.floor((durationS - (days*_S_PER_DAYS)) / _S_PER_HOURS )
  const mins = Math.round((durationS - (days*_S_PER_DAYS) - (hours*_S_PER_HOURS)) / _S_PER_MIN)
  let message = (days > 0) ? `${days} days` : ''
  message = (hours > 0) ? `${message} ${hours} hours` : message
  message = (mins > 0) ? `${message} ${mins} min` : message
  console.log("durationS: ", durationS, "  message: ", message)
  return message
}


function createTableFromRunningServersJson(jsonInput) {
  // Convert json data to HTML table to insert into "running-servers" div
  const targetDiv = document.getElementById("running-servers")
  const table = document.createElement("table")
  const columns = Object.keys(jsonInput[0])
  const header = document.createElement("header")
  const tr = document.createElement("tr")

  columns.forEach((item) => {
    let th = document.createElement("th")
    th.innerText = item
    tr.appendChild(th)
  })

  header.appendChild(tr)
  table.append(tr)

  jsonInput.forEach((item) => {
    let tr = document.createElement("tr")
    const values = Object.values(item)
    values.forEach((elem) => {
      let td = document.createElement("td")
      td.innerText = elem
      tr.appendChild(td)
    })
    table.appendChild(tr)
  })
  targetDiv.appendChild(table)
}

// HTML rendering
//=====================

function refreshHTML(){
  RenderChart()
  RenderTable()
}

function RenderChart(type) {
  TODAY_CHART.updateSeries(getDataSeries(CHART_DATA, "by_day_consumption"))
  MONTH_CHART.updateSeries(getDataSeries(CHART_DATA, "this_month_consumption"))
  SLIDING_MONTH_CHART.updateSeries(getDataSeries(CHART_DATA, "one_sliding_month_consumption"))
}

function RenderTable() {
  document.getElementById("running-server-title").textContent = `Running servers at ${RUNNING_SERVER_DATA["date"]}`
  const serverData = formatRunningServerData()
  createTableFromRunningServersJson(serverData)
}

// Chart configuration
//=======================
function getChartOptions(title, color){
  const options = {
    colors: [color],
    series: [],
    chart: {
      type: 'bar',
      height: 700,
      stacked: false,
      zoom: {
        type: 'x',
        enabled: true,
        autoScaleYaxis: false
      },
      toolbar: {
        autoSelected: 'pan'
      }
    },
    stroke: {
      curve: 'smooth',
    },
    dataLabels: {
      enabled: false
    },
    markers: {
      size: 0,
    },
    title: {
      text: title,
      align: 'center',
      style: {
        fontSize:  '24px'
      }
    },
    yaxis: {
      title: {
        text: 'cost ($)',
        rotate: 270,
        style: {
              color: undefined,
              fontSize: '18px'
        }
      },
      min: 0
    },
    xaxis: {
      type: 'datetime'
    },
    legend: {
      show: false,
      showForSingleSeries: true,
      position: 'top',
      horizontalAlign: 'left'
    },
    noData: {
      text: 'No data to display...',
      style: {
        fontSize: '24px'
      }
    }
  }
  return options
}
