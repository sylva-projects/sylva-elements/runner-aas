import os
import pytest
from unittest.mock import patch, MagicMock
from .equinix import EquinixWrapper


@pytest.fixture
def equinix_wrapper():
    with patch.dict(os.environ, {"EQUINIX_PROJECT_ID": "test_project_id", "EQUINIX_API_KEY": "test_api_key"}):
        yield EquinixWrapper()


def test_get_servers_success(equinix_wrapper):
    with patch('requests.get') as mock_get:
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {"devices": [{"hostname": "test-server"}]}
        mock_get.return_value = mock_response
        servers = equinix_wrapper.get_servers()
        assert len(servers["devices"]) == 1
        assert servers["devices"][0]["hostname"] == "test-server"


def test_get_servers_failure(equinix_wrapper):
    with patch('requests.get') as mock_get:
        mock_response = MagicMock()
        mock_response.status_code = 500
        mock_response.text = "Internal Server Error"
        mock_get.return_value = mock_response
        with pytest.raises(SystemExit):
            equinix_wrapper.get_servers()
