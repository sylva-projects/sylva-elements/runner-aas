#!/usr/bin/env python3

# Common functions using Gitlab API

import gitlab
import os
import sys
from gitlab.exceptions import GitlabGetError


class GitlabWrapper:

    common_tags = {"equinix", "temporary"}

    def __init__(self, project):
        token_var_name = f"{project.split('/')[-1].upper().replace('-', '_')}_RUNNER_MGMT_TOKEN"
        token = os.getenv(token_var_name)
        if not token:
            raise RuntimeError("Credential not found")
        self.gl = gitlab.Gitlab(private_token=token)
        self.gl.auth()

    def get_runners(self):
        all_runners = self.gl.runners.list(get_all=True)
        runners = []
        for r in all_runners:
            try:
                runner = self.gl.runners.get(r.id)
                if self.common_tags.issubset(set(runner.tag_list)):
                    runners.append(runner)
            except GitlabGetError:  # in some edge cases, runner may have been deleted between runner list retrieval and actual inspection
                pass
        return runners

    def get_runner_id_by_name(self, name):
        runners = self.get_runners()
        for runner in runners:
            if runner.description == name:
                return runner.id

    def create_runner(self, name, project_id, tags):
        runner = self.gl.user.runners.create({
            "runner_type": "project_type",
            "project_id": project_id,
            "description": name,
            "locked": True,
            "run_untagged": False,
            "tag_list": list(self.common_tags) + tags,
            "access_level": "not_protected",
            "maximum_timeout": 10800,
        })
        return runner

    def unregister_runner_by_id(self, id):
        self.gl.runners.delete(id)

    def is_pipeline_running(self, project_id, pipeline_id):
        try:
            project = self.gl.projects.get(project_id)
            status = project.pipelines.get(pipeline_id).status
            if status == "running" or status == "pending" or status == "scheduled":
                return True
        except Exception as e:
            print(f"Unable to get status for pipeline {pipeline_id}: {e}", file=sys.stderr)
            return True
        return False
