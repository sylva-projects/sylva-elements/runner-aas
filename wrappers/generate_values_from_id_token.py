#!/usr/bin/env python3

import jwt
import os
import requests
from .common import fail, ALLOWED_PROJECTS

"""
This script takes a Gitlab ID token as parameter and does the following:
  * Verify that ID token is valid
  * Verify that ID token what generated for runner creation/deletion context (aud='runner-aas')
  * Verify that ID token is issued by an allowed project (see ALLOWED_PROJECTS below)
  * Generates a dotenv file (see VALUE_FILE_PATH) containing runners parameters:
        - RUNNER_HOSTNAME
        - RUNNER_PROJECT
        - RUNNER_PROJECT_ID
        - RUNNER_PARENT_PIPELINE_IID
All these parameters are created from data encoded into ID token
and will be used to create requested runner
"""

VALUE_FILE_PATH = "runner_details.env"


def get_gitlab_signing_keys(id_token):
    jwks_client = jwt.PyJWKClient("https://gitlab.com/oauth/discovery/keys")
    return jwks_client.get_signing_key_from_jwt(id_token)


def validate_and_get_token_payload(id_token, signing_key):
    payload = jwt.decode(
        id_token,
        key=signing_key.key,
        audience="runner-aas",
        algorithms=["RS256"],
    )

    if not payload["project_path"] in ALLOWED_PROJECTS:
        fail("Access not allowed")

    print(f"Access granted for user {payload['user_login']} from {payload['sub']}")
    return payload


def generate_value_file_from_id_token(payload):
    # Write runner parameters in an dotenv file
    RUNNER_PROJECT = payload["project_path"].split("/")[-1]
    RUNNER_PROJECT_ID = payload["project_id"]
    RUNNER_HOSTNAME = f"{RUNNER_PROJECT}-{payload['pipeline_id']}"
    RUNNER_PARENT_PIPELINE_IID = get_pipeline_iid(payload)
    with open(VALUE_FILE_PATH, "w") as f:
        f.write(f"RUNNER_HOSTNAME={RUNNER_HOSTNAME}\n")
        f.write(f"RUNNER_PROJECT={RUNNER_PROJECT}\n")
        f.write(f"RUNNER_PROJECT_ID={RUNNER_PROJECT_ID}\n")
        f.write(f"RUNNER_PARENT_PIPELINE_IID={RUNNER_PARENT_PIPELINE_IID}\n")


def get_pipeline_iid(payload):
    url = f"{os.getenv('CI_API_V4_URL')}/projects/{payload['project_id']}/pipelines/{payload['pipeline_id']}"
    runner_mgmt_token_name = f"{payload['project_path'].split('/')[-1].replace('-', '_')}_RUNNER_MGMT_TOKEN"
    headers = {"PRIVATE-TOKEN": os.getenv(runner_mgmt_token_name)}
    r = requests.get(url, headers=headers)
    json_response = r.json()
    if r.status_code != 200 or "iid" not in json_response:
        print(r)
        print(json_response)
        fail("Unable to retrieve initial pipeline IID")
    return json_response["iid"]
