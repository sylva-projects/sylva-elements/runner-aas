import sys


PROTECTED_VARIABLES = {
    "EQUINIX_PROJECT_ID",
    "EQUINIX_API_KEY",
    "SYLVA_CORE_RUNNER_MGMT_TOKEN",
    "KIWI_IMAGEBUILDER_RUNNER_MGMT_TOKEN",
}

ALLOWED_PROJECTS = {
    "sylva-projects/sylva-core",
    "sylva-projects/sylva-elements/kiwi-imagebuilder",
}


def fail(msg, code=1):
    print(msg, file=sys.stderr)
    sys.exit(code)
