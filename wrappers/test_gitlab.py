import os
import pytest
from unittest.mock import patch, MagicMock
from .gitlab import GitlabWrapper


@pytest.fixture
def gitlab_wrapper():
    project_name = "test-project"
    with patch.dict(os.environ, {"TEST_PROJECT_RUNNER_MGMT_TOKEN": "test_token"}):
        with patch('gitlab.Gitlab'):
            wrapper = GitlabWrapper(project_name)

            mock_runner1 = MagicMock()
            mock_runner1.id = 123
            mock_runner1.description = "test-runner"
            mock_runner1.tag_list = ["equinix", "temporary", "tag1", "tag2"]

            mock_runner2 = MagicMock()
            mock_runner2.id = 456
            mock_runner2.description = "other-test-runner"
            mock_runner2.tag_list = ["other_tag"]

            wrapper.gl.runners.list.return_value = [mock_runner1, mock_runner2]
            wrapper.gl.runners.get.side_effect = lambda id: {123: mock_runner1, 456: mock_runner2}[id]

            yield wrapper


def test_get_runners_success(gitlab_wrapper):
    runners = gitlab_wrapper.get_runners()
    assert len(runners) == 1
    assert runners[0].id == 123


def test_get_runner_id_by_name_success(gitlab_wrapper):
    runner_id = gitlab_wrapper.get_runner_id_by_name("test-runner")
    assert runner_id == 123
    not_found_runner_id = gitlab_wrapper.get_runner_id_by_name("other-runner")
    assert not_found_runner_id is None
