#!/usr/bin/env python3

# Common functions using Equinix Metal API

import os
import random
import requests
import time
from datetime import datetime, timedelta, timezone
from .common import fail


class EquinixWrapper:
    API_URL = "https://api.equinix.com/metal/v1"

    def __init__(self):
        self.PROJECT_ID = os.getenv("EQUINIX_PROJECT_ID")
        self.API_KEY = os.getenv("EQUINIX_API_KEY")
        self.headers = {"X-Auth-Token": self.API_KEY}

        if not self.PROJECT_ID or not self.API_KEY:
            fail("Credential not found")

    def get_servers(self):
        url = f"{self.API_URL}/projects/{self.PROJECT_ID}/devices?per_page=500"
        response = requests.get(url, headers=self.headers)
        if response.status_code != 200:
            fail(f"Unable to get servers list: got {response}\n{response.text}")
        return response.json()

    def get_server_by_name(self, name):
        servers = self.get_servers()
        for server in servers["devices"]:
            if server["hostname"] == name:
                return server

    def get_server_id_by_name(self, name):
        server = self.get_server_by_name(name)
        if server:
            return server["id"]

    def create_server(self, name, plan, operating_system, termination_delay):
        metro = self.find_best_metro(plan)
        print(f"    >> Selected metro: {metro['name']} ")
        url = f"{self.API_URL}/projects/{self.PROJECT_ID}/devices"
        user_data = self._generate_user_data("user-data.sh.tpl")
        termination_time = datetime.now(timezone.utc) + timedelta(hours=termination_delay)
        request_data = {
            "metro": metro["code"],
            "plan": plan,
            "operating_system": operating_system,
            "hostname": name,
            "userdata": user_data,
            "termination_time": termination_time.isoformat(timespec="minutes"),
        }
        response = requests.post(url, headers=self.headers, json=request_data)
        if response.status_code != 201:
            fail(f"Unable to create server: got {response}\n{response.text}")
        return response.json()

    def _generate_user_data(self, file):
        with open(file, "r") as f:
            user_data_tpl = f.read()
        user_data = (
            user_data_tpl.replace("%%CI_SERVER_HOST%%", os.getenv("CI_SERVER_HOST"))
            .replace("%%RUNNER_HOSTNAME%%", os.getenv("RUNNER_HOSTNAME"))
            .replace("%%GITLAB_RUNNER_SHELL_TOKEN%%", os.getenv("GITLAB_RUNNER_SHELL_TOKEN"))
            .replace("%%GITLAB_RUNNER_DOCKER_TOKEN%%", os.getenv("GITLAB_RUNNER_DOCKER_TOKEN"))
            .replace("%%RUNNER_PLAN%%", os.getenv("RUNNER_PLAN"))
        )
        return user_data

    def _get_server_state(self, name):
        server = self.get_server_by_name(name)
        if server:
            return server["state"]
        else:
            return "N/A"

    def wait_server_to_be_ready(self, name):
        while True:
            state = self._get_server_state(name)
            print(f"    >> Current server state is: {state}")
            if state == "active":
                break
            time.sleep(5)

    def _wait_server_to_be_deletable(self, name):
        while True:
            state = self._get_server_state(name)
            if state != "N/A" and state != "provisioning" and state != "queued":
                break
            print(f"    >> Current server state is: {state}")
            time.sleep(5)

    def delete_server_by_name(self, name):
        id = self.get_server_id_by_name(name)
        if not id:
            print("Server not found")
            return
        self._wait_server_to_be_deletable(name)
        url = f"{self.API_URL}/devices/{id}?force_delete=true"
        response = requests.delete(url, headers=self.headers)
        if response.status_code != 204:
            fail(f"Unable to delete server: got {response}\n{response.text}")

    def check_plan_available_in_metro(self, plan, metro):
        url = f"{self.API_URL}/capacity/metros"
        request_data = {
            "servers": [{
                "metro": metro["code"],
                "plan": plan,
                "quantity": "1",
            }]
        }
        response = requests.post(url, headers=self.headers, json=request_data)
        if response.status_code != 200:
            fail(f"Unable to get equinix capacity: got {response}\n{response.text}")
        if response.json()["servers"][0]["available"]:
            return True
        return False

    def find_best_metro(self, plan):
        # find a metro where plan is available at the lower price
        metros_by_price = self._find_metros_with_better_price(plan)
        for metro in metros_by_price:
            if self.check_plan_available_in_metro(plan, metro):
                return metro
        fail(f"No metro available for plan {plan}")

    def _find_metros_with_better_price(self, plan):
        # Equinix asks us to pick our servers into their biggest metros
        allowed_metros = {
            "dc",
            "da",
            "sv",
            "am",
        }
        url = f"{self.API_URL}/projects/{self.PROJECT_ID}/plans?include=available_in_metros"
        response = requests.get(url, headers=self.headers)
        if response.status_code != 200:
            fail(f"Unable to get equinix plan info: got {response}\n{response.text}")
        plan = next(p for p in response.json()["plans"] if p["name"] == plan)
        availability = plan["available_in_metros"]
        random.shuffle(availability)  # shuffle list of metro to have randomization in lower price metros
        return sorted(
            [metro for metro in availability if metro["code"] in allowed_metros and "price" in metro and "hour" in metro["price"]],
            key=lambda m: (m["price"]["hour"]),
            reverse=False,
        )

    def get_total_project_usage(self, since_date):
        today = datetime.now(timezone.utc).strftime('%Y-%m-%d')
        since = since_date.strftime('%Y-%m-%d')
        url = f"{self.API_URL}/projects/{self.PROJECT_ID}/usages?created[after]={since}T00:00:00&created[before]={today}T23:59:59&per_page=1000"
        response = requests.get(url, headers=self.headers)
        if response.status_code != 200:
            fail(f"Unable to get equinix project usage: got {response}\n{response.text}")
        return sum([x["total"] for x in response.json()["usages"]])
