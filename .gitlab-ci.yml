---
workflow:
  name: $PIPELINE_NAME
  rules:
    - if: $CI_DEBUG_TRACE
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true"
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

stages:
  - lint
  - creation
  - deletion
  - report
  - test

include:
  - project: 'sylva-projects/sylva-elements/ci-tooling/ci-templates'
    ref: 1.0.37
    file:
      - 'templates/release-notes.yml'

default:
  image:
    name: registry.gitlab.com/python-gitlab/python-gitlab:v4.13.0-alpine
    entrypoint: [""]
  cache:
    key: $CI_JOB_NAME
    paths:
      - .cache/pip

variables:
  # Override for release notes template, to exclude test tag.
  GIT_DESCRIBE_OPTIONS: --exclude "0.0.0* --exclude "test"
  EXCLUDED_CI_COMMIT_TAG: /0\.0\.0.*|test/

  PIPELINE_NAME: "Request $REQUEST from $CI_PIPELINE_SOURCE"
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  REQUEST:
    description: "Describe what the pipeline should do"
    value: ""
    options:
      - ""
      - "create"
      - "delete"
      - "wait"
      - "periodic_clean"
      - "report"
  RUNNER_PLAN:
    description: "Required server plan (flavor) for runner (see https://deploy.equinix.com/product/bare-metal/servers)"
    value: "c3.small.x86"
    options:
      - "c3.small.x86"
      - "m3.small.x86"
      - "c3.medium.x86"
      - "m3.large.x86"
  RUNNER_OS:
    description: "Required server OS for runner"
    value: "ubuntu_22_04"
    options:
      - "ubuntu_22_04"
  RUNNER_ADDITIONAL_TAGS:
    description: "Optional comma separated list of tags expected on runner"
    value: ""
    # expand: false

checks:
  stage: .pre
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "api" || $CI_PIPELINE_SOURCE == "pipeline"
  script:
    - >
      [ -z "$SOURCE_JWT_TOKEN" ] && { echo >&2 "SOURCE_JWT_TOKEN is required to run this pipeline"; exit 1; }
    - ./checks.py sanitize_env
    - pip install pyjwt[crypto] requests --quiet --quiet
    - ./checks.py generate_env_from_id_token
    - cat runner_details.env
  artifacts:
    reports:
      dotenv: runner_details.env

get_running_servers:
  stage: .pre
  rules:
    - if: $GITLAB_CI == "false"  # local testing only
    - if: $REQUEST == "create"
  script:
    - ./manage_runner.py get_equinix_servers

get_runners:
  stage: .pre
  rules:
    - if: $GITLAB_CI == "false"  # local testing only
  script:
    - ./manage_runner.py get_gitlab_runners

test_find_metro:
  stage: .pre
  rules:
    - if: $GITLAB_CI == "false"  # local testing only
  script:
    - ./manage_runner.py test

pylama:
  script:
    - pip install setuptools --root-user-action=ignore --quiet
    - pip install pylama==8.4.1 --root-user-action=ignore --quiet
    - pylama .
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

unit-tests:
  script:
    - pip install pytest --root-user-action=ignore --quiet
    - pytest -v --junitxml=report.xml
  artifacts:
    reports:
      junit:
        - report.xml
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

create:
  stage: creation
  rules:
    - if: $REQUEST == "create"
  resource_group: "create_runner"  # ensure only one creation request is handled at a time
  script:
    - if [ -z "$RUNNER_ADDITIONAL_TAGS" ]; then export RUNNER_ADDITIONAL_TAGS=${RUNNER_PARENT_PIPELINE_IID}; fi
    - ./manage_runner.py create_runner

wait:
  needs:
    - job: create
    - job: checks
      optional: true
  stage: creation
  timeout: 30min
  rules:
    - if: $REQUEST == "create"
      when: delayed
      start_in: 140 seconds
    - if: $REQUEST == "wait"
  script:
    - if [ -z "$RUNNER_HOSTNAME" ]; then echo >&2 "RUNNER_HOSTNAME not defined"; exit 1; fi
    - echo "Wait server $RUNNER_HOSTNAME to be ready"
    - ./manage_runner.py wait_server_to_be_ready

unregister:
  stage: deletion
  rules:
    - if: $REQUEST == "delete"
  script:
    - ./manage_runner.py unregister_runner

delete:
  stage: deletion
  rules:
    - if: $REQUEST == "delete"
  script:
    - ./manage_runner.py delete_server

delete_old_runners:
  stage: deletion
  rules:
    - if: $REQUEST == "periodic_clean"
  script:
    - ./manage_runner.py delete_old_runners

delete_old_servers:
  stage: deletion
  rules:
    - if: $REQUEST == "periodic_clean"
  script:
    - ./manage_runner.py delete_old_servers

get_usage:
  stage: report
  rules:
    - if: $REQUEST == "report"
  script:
    - |
      echo "Usage follow-up:"
      ./manage_runner.py display_total_equinix_usage > current_consumption.json
    - |
      echo "Running servers:"
      ./manage_runner.py get_equinix_servers | tee running_servers.json
  artifacts:
    paths:
      - current_consumption.json
      - running_servers.json

pages:
  stage: report
  rules:
    - if: $REQUEST == "report"
  needs: [get_usage]
  before_script:
    - apk add curl jq --quiet
  script:
    - cp -r web-report/ public/
    - |
      if [ $(curl -Ss "$CI_PAGES_URL/consumption_history.json" -o consumption_history.json -w '%{http_code}\n' -s) != "200" ]; then
        echo >&2 "Unable to retrieve current published history"
        exit 1
      fi
    - jq -s '.[0] * .[1]' consumption_history.json current_consumption.json > public/consumption_history.json
    - cp running_servers.json public/running_servers_report.json
  artifacts:
    paths:
      - public/
