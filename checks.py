#!/usr/bin/env python3

import argparse
import os
import sys
from wrappers.common import fail, PROTECTED_VARIABLES


def env_variables_containing(protected_variable_name):
    vars = []
    for var in os.environ:
        if os.environ[protected_variable_name] in os.environ[var]:
            vars.append(var)
    return vars


def generate_env_from_id_token():
    from wrappers.generate_values_from_id_token import get_gitlab_signing_keys, validate_and_get_token_payload, generate_value_file_from_id_token
    id_token = os.getenv("SOURCE_JWT_TOKEN")
    signing_key = get_gitlab_signing_keys(id_token)
    payload = validate_and_get_token_payload(id_token, signing_key)
    generate_value_file_from_id_token(payload)


def sanitize_env():
    """
    This script verifies that any protected variable content is not copied in another env variable
    """
    for protected_var in PROTECTED_VARIABLES:
        if os.getenv(protected_var):
            if (env_vars_containing_protected_var := env_variables_containing(protected_var)) != [protected_var]:
                fail(f"Unexpected environment: {env_vars_containing_protected_var}", file=sys.stderr)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Verify pipeline environment")
    parser.add_argument(
        "action",
        choices=[
            "generate_env_from_id_token",
            "sanitize_env",
        ],
    )
    args = parser.parse_args()

    if args.action == "generate_env_from_id_token":
        generate_env_from_id_token()
    if args.action == "sanitize_env":
        sanitize_env()
